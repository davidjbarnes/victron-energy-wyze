import { Controller, Get } from '@nestjs/common';
import { AppService, Device, Summary } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('summary')
  getSummary(): Promise<Summary> {
    return this.appService.getSummary();
  }

  @Get('devices')
  getDevices(): Promise<Device[]> {
    return this.appService.getWyzeDevices();
  }

}
