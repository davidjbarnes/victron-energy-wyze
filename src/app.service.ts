import { Inject, Injectable, Logger } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { MqttService, Payload, Subscribe } from 'nest-mqtt';
import * as Wyze from 'wyze-node';

const minBatteryVoltage: number = 13.25;
const minPvWatts: number = 200;
const fridgeMac: string = '2CAA8E729784';

export class Device {
  mac: string;
  name: string;
  state: number;

  constructor(mac: string, name: string, state: number) {
    this.mac = mac;
    this.name = name;
    this.state = state;
  }
}

export class Summary {
  acLoad: string;
  batteryVoltage: number;
  pvWatts: number;
  relay0State: number;
  minBatteryVoltage: number;
  minPvWatts: number;
  fridgeState: string;

  constructor(_minBatteryVoltage: number, _minPvWatts: number) {
    this.minBatteryVoltage = _minBatteryVoltage;
    this.minPvWatts = _minPvWatts;
  }
}

interface Measurement {
  name: string;
  topic: string;
}

const options = {
  username: process.env.WYZE_USERNAME,
  password: process.env.WYZE_PASSWORD
}

const measurements: Array<Measurement> = [
  {name: 'acLoad', topic: 'N/48e7da886f65/system/0/Ac/Consumption/L1/Power'},
  {name: 'batteryVoltage', topic: 'N/48e7da886f65/system/0/Dc/Battery/Voltage'},
  {name: 'pvWatts', topic: 'N/48e7da886f65/solarcharger/277/Yield/Power'},
  {name: 'relay0State', topic: 'N/48e7da886f65/system/0/Relay/0/State'}
];

@Injectable()
export class AppService {
  private readonly logger = new Logger(AppService.name);
  private summary: Summary = new Summary(minBatteryVoltage, minPvWatts);
  private wyze: any = new Wyze(options);

  constructor(
    @Inject(MqttService) private readonly mqttService: MqttService,
  ) {
    this.wake();
  }
  
  @Cron(CronExpression.EVERY_MINUTE)
  handleStayAlive() {
    this.wake();
  }

  @Cron(CronExpression.EVERY_MINUTE)
  async getDeviceState() {
    await this.getWyzeDevices();
    await this.evalFridgeState();
  }

  async getSummary(): Promise<Summary> {
    return this.summary;
  }

  async wake(): Promise<void> {
    this.logger.debug('waking victron...');
    measurements.forEach(m => this.mqttService.publish(m.topic.replace('N','R'), ''));
  }

  async getWyzeDevices(): Promise<Device[]> {
    let devices:Array<Device> = new Array<Device>();

    const list = await this.wyze.getDeviceList(); 
    for(const d of list) {
      const state = await this.wyze.getDeviceStatus(d);
      const device:Device = new Device(d.mac, d.nickname, state.switch_state);
      devices.push(device);
    }

    return devices;
  }

  async toggleDevice(mac:string, state:number): Promise<void> {
    const device = await this.wyze.getDeviceByMac(mac);
    const result = state == 1 ? await this.wyze.turnOn(device) : await this.wyze.turnOff(device);
  }

  async evalFridgeState(): Promise<void> {
    this.logger.debug(`evalFridgeState:`)

    if(this.summary.pvWatts > minPvWatts && this.summary.batteryVoltage > minBatteryVoltage && this.summary.relay0State == 1) {
      await this.toggleDevice(fridgeMac, 1);
      this.logger.debug(`relay0State=${this.summary.relay0State}, pvWatts=${this.summary.pvWatts}, batteryVoltage=${this.summary.batteryVoltage}, fridge=ON`)
    } else {
      await this.toggleDevice(fridgeMac, 0);
      this.logger.debug(`relay0State=${this.summary.relay0State}, pvWatts=${this.summary.pvWatts}, batteryVoltage=${this.summary.batteryVoltage}, fridge=OFF`)
    }
  }

  @Subscribe(measurements[0].topic)
  handleAcLoad(@Payload() payload) {
    this.summary.acLoad = payload.value;
    this.logger.debug(`summary: ${JSON.stringify(this.summary)}`);
  }

  @Subscribe(measurements[1].topic)
  handleBatteryVoltage(@Payload() payload) {
    this.summary.batteryVoltage = payload.value;
    this.logger.debug(`summary: ${JSON.stringify(this.summary)}`);
  }

  @Subscribe(measurements[2].topic)
  handlePvWatts(@Payload() payload) {
    this.summary.pvWatts = payload.value;
    this.logger.debug(`summary: ${JSON.stringify(this.summary)}`);
  }

  @Subscribe(measurements[3].topic)
  handleRelay0State(@Payload() payload) {
    this.summary.relay0State = payload.value;
    this.logger.debug(`summary: ${JSON.stringify(this.summary)}`);
  }
}
